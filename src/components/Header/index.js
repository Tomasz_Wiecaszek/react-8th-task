import React from "react";
import './style.css'

const scrollToRef=ref=>window.scrollTo(0,ref.current.offsetTop);

const Header=({refPol,refLas,refDżu,refPus,refGór})=>{
    const navList=[
        {name:'Polana',ref:refPol},
        {name:'Las',ref:refLas},
        {name:'Dżungla',ref:refDżu},
        {name:'Pustynia',ref:refPus},
        {name:'Góry',ref:refGór},
    ];
    return(
        <div className="nav-wrapper">
            <ul className="nav">
        
                {navList.map(el=>{return(
                    <li key={el.name} onClick={()=>scrollToRef(el.ref)}>{el.name}</li>
                )})}
                
            </ul>
        </div>
    )
}
export default Header;