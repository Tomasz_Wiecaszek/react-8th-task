import {CharStats} from "../../context"
import React, {useContext} from 'react'
import './style.css'


const RandomStats =()=>{
let {setStr,setHp,setSpeed,setLvl}=useContext(CharStats);


function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    return Math.floor(Math.random() * (max - min)) + min;
  }


const randomStatsFunction=()=>{
    setStr(getRandomInt(1,10));
    setHp(getRandomInt(1,100));
    setSpeed(getRandomInt(1,10));
    setLvl(getRandomInt(1,10));
}

return(
    <button className="button" onClick={randomStatsFunction}>CLICK TO GET RANDOM STATS OF OPONENT</button>
)
}

export default RandomStats