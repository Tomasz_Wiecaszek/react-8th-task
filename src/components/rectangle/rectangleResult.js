
import {React, useEffect, useState,useCallback} from 'react'

const RectangleResult=({myRef})=>{
    

let [result,setResult]=useState(0);

const [, updateState] = useState();
const forceUpdate = useCallback(() => updateState({}), []);

    useEffect(() => {
        setResult(result=myRef.current.clientWidth*myRef.current.clientHeight);
        forceUpdate();
      });



return(
    <div ref={myRef}> Powierzchnia:  {result} </div>
)
}

export default RectangleResult;
