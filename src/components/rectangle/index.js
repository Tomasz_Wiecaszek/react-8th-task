
import React, {useRef, useState} from 'react'
import styled from 'styled-components'
import RectangleResult from './rectangleResult'
import './style.css'

const MyRectangle = styled.div`
width:${props => props.width+'px'};
height: ${props => props.height+'px'};
background-color:green;
disply:flex;
text-align:center;
align-items:center;
justify-content:center;
transition:all 0.5s;
`;

// class Rectangle extends Component{
//     constructor(){
//         super();
//         this.state={
//             width:103,
//             height:120,
//             max:500,
//             min:100
//         }
//     }
//     handleResize = ()=>{
//         const max=this.state.max;
//         const min=this.state.min;
//         let width = Math.floor(Math.random()*(max-min))+min;
//         let height = Math.floor(Math.random()*(max-min))+min;
//         this.setState({
//             width,
//             height
//         });
//     }


// render(){
//     let {width,height}=this.state;
//     const ref = React.createRef();
    
//     return(
//         <div className="rectangle__wrapper">
//             <MyRectangle ref={ref} width={width} height={height}>
          
//                 <RectangleResult {...{ref}}/>
//             </MyRectangle>

//                         <p>Szerokość {width} px</p>
//                         <p>Wysokość {height} px</p>
//             <button onClick={()=>this.handleResize()}>Generuj</button>
//         </div>
//     )
// }
// }

// export default Rectangle;

const Rectangle=()=>{
const max=500;
const min=100;

let [width,setWidth]=useState(100);
let [height,setHeight]=useState(120);
const myRef=useRef(null);
    const handleResize = ()=>{
        setWidth(width=Math.floor(Math.random()*(max-min))+min);
        setHeight(height=Math.floor(Math.random()*(max-min))+min);
    }
    return(
 <div className="rectangle__wrapper">
            <MyRectangle ref={myRef} width={width} height={height}>
          
                <RectangleResult {...{myRef}}/>
            </MyRectangle>

                        <p>Szerokość {width} px</p>
                        <p>Wysokość {height} px</p>
           <button onClick={()=>handleResize()}>Generuj</button>
       </div>
    )
}

export default Rectangle;