import './style.css'
import TableRows from '../../components/TableRows'
import {CharStats} from "../../context"
import React, {useContext, useEffect} from 'react'

const OponentStats =(props)=>{
    let alive=props.alive;
    let {name,str,hp,speed,dmg,addName,lvl}=useContext(CharStats);
    const myStats={name,str,hp,speed,dmg,lvl,alive};
useEffect(()=>{
    if(!alive){
    addName(localStorage.getItem("name"));}
},);
    return(
<div className="col-6">
    {
        props.render(myStats)
        }
    </div>
    )}

export default OponentStats;